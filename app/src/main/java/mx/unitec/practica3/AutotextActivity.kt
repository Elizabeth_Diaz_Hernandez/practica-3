package mx.unitec.practica3

import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.text.AutoText
import android.widget.ArrayAdapter
import android.widget.AutoCompleteTextView
import android.widget.Toast

class AutotextActivity : AppCompatActivity() {

    lateinit var autoTextView: AutoCompleteTextView

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_autotext)

        autoTextView = findViewById(R.id.autoCompleteTextViewAlcaldía)

        val adapter = ArrayAdapter.createFromResource(
            this,
            R.array.alcaldia_array,
            android.R.layout.select_dialog_item
        )

        autoTextView.threshold = 2

        autoTextView.setAdapter(adapter)

        autoTextView.setOnItemClickListener { parent, view, position, id ->
            Toast.makeText(this,
            position.toString() + ": " + parent?.getItemIdAtPosition(position).toString(),
            Toast.LENGTH_SHORT).show()
        }
    }
}