package mx.unitec.practica3

import android.app.TimePickerDialog
import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.view.View
import kotlinx.android.synthetic.main.activity_picker_time.*
import mx.unitec.practica3.ui.TimePickerFragment

class PickerTimeActivity : AppCompatActivity() {
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_picker_time)
    }

    fun showDatePickerDialog(v: View) {
   //val  timePickerFragment= TimePickerFragment()

        val timePickerFragment = TimePickerFragment.newIntance(TimePickerDialog.OnTimeSetListener
        { view, hourOfDay, minute ->
            pkrTime.setText("${hourOfDay}: ${minute}")

        })

        timePickerFragment.show(supportFragmentManager, "timePicker")

    }
}